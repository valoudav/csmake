#!/usr/bin/env python3
# author: David Valouch
# mail: valoudav@fel.cvut.cz

from utils import print_warn

def compute_eval(tokens, config):
    categories = config['penal_code']
    
    max_sev = {}
    for t in filter(lambda t: t['type'] == 'eval', tokens):
        cat = t['category']
        sev = t['severity']
        if cat not in categories.keys():
            print_warn(f'Unknown category: {cat} ({t["file"]}:{t["linum"]})')
        elif cat in max_sev:
            max_sev[cat] = max(sev, max_sev[cat])
        else:
            max_sev[cat] = sev

    penalty = 0.0
    for c, ms in max_sev.items():
        w = categories[c]['max_penalty']
        penalty += w * ms
    penalty = min(penalty, 1.0)

    return penalty, max_sev

def output_txt(tokens, config, path):
    categories = config['penal_code']

    penalty, max_sev = compute_eval(tokens, config)

    with open(path, 'w') as of:
        of.write(' CODING STYLE EVALUATION '.center(60, '=') + '\n')

        of.write(f'TOTAL PENALTY: -{penalty*100} %\n')

        of.write(''.center(60, '-') + '\n')
        of.write('max penalty per category:\n')

        for k, v in max_sev.items():
            name = categories[k]['name']
            w = categories[k]['max_penalty']
            of.write(f'  {name}: -{int(w*v*100)} %\n')

        of.write(''.center(60, '-') + '\n')

        for t in tokens:
            kind = t['type']
            cat = t['category'] if kind == 'eval' else None
            name = categories[cat]['name'] if kind == 'eval' else ''
            w = categories[cat]['max_penalty'] if kind == 'eval' else None
            linum = t['linum']
            file = t['file']
            comment = t['comment'] if kind == 'eval' else t['raw_token']
            t_penalty = f' -{int(w * t["severity"] * 100)}% -- ' if kind == 'eval' else ''

            of.write(f'{file}:{linum} -- {name}{t_penalty}{comment}\n')
    
    return penalty, max_sev


