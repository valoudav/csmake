#!/usr/bin/env python3
# author: David Valouch
# mail: valoudav@fel.cvut.cz

import os, re, yaml
from utils import print_warn

COMMENT_REGEX = r'//(.*)'
TOKEN_REGEX = r'\$([^$^\n]*)'
EVAL_REGEX = r'([\d]*)[ ]*;[ ]*([\d]*.?[\d]*)[ ]*;(.*)'
EVAL_CHECK_REGEX = r'.*;.*'
SOURCE_FILE_REGEX = r'.*(\.h|\.c)'

def parse_lines(infile, filename):
    tokens = []
    for linum, line in enumerate(infile, start=1):
        res = re.findall(COMMENT_REGEX, line)
        res = re.findall(TOKEN_REGEX, res[0]) if res else []
        res = [ r.strip() for r in res ]
        for token in res:
            tokens.append({'linum' : linum, 'file' : filename , 'raw_token' : token})
    return tokens

def parse_file(path):
    with open(path, 'r') as infile:
        tokens = parse_lines(infile, path)
    return tokens

def parse_dir(path):
    files = os.listdir(path)
    files = list(filter(lambda f : re.match(SOURCE_FILE_REGEX, f), files))

    tokens = []
    for f in files:
        tokens += parse_file(os.path.join(path, f))
    return tokens

def parse_token(token):
    res = re.findall(EVAL_REGEX, token['raw_token'])
    if res:
        category = res[0][0]
        severity = res[0][1]
        comment = res[0][2]

        category = category.strip()

        severity = float(severity) if severity else 0.0
        if not 0.0 <= severity <= 1.0:
            print_warn(f'Severity value out of range in "{token["raw_token"]}". Defaulting to 0. ({token["file"]}:{token["linum"]})')
            severity = 0.0

        comment = comment.strip()

        token['type'] = 'eval'
        token['category'] = category
        token['severity'] = severity
        token['comment'] = comment
    else:
        if re.match(EVAL_CHECK_REGEX, token['raw_token']):
            print_warn(f'Possibly mangled token: "{token["raw_token"]}" ({token["file"]}:{token["linum"]})')
        token['type'] = 'comment'

def save_tokens(tokens, path):
    with open(path, 'w') as outfile:
        yaml.dump(tokens, outfile)

def load_tokens(path):
    try:
        with open(path, 'r') as infile:
            tokens = yaml.safe_load(infile)
    except:
        tokens = None

    return tokens
