#!/usr/bin/env python3
# author: David Valouch
# mail: valoudav@fel.cvut.cz

import sys
from colorama import Fore, Style

def print_warn(message):
    print(f'[{sys.argv[0]}] ' + Fore.YELLOW + 'WARNING:' + Style.RESET_ALL + f' {message}', file=sys.stderr) 

def print_err(message):
    print(f'[{sys.argv[0]}] ' + Fore.RED + 'ERROR:' + Style.RESET_ALL + f' {message}', file=sys.stderr) 
