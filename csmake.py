#!/usr/bin/env python3
# author: David Valouch
# mail: valoudav@fel.cvut.cz

import os, sys, re, yaml, csv, subprocess

from utils import print_err, print_warn
import token_parsing as tp
import result_processing as rp

ERROR_UNKNOWN_COMMAND = 200
ERROR_MISSING_CONFIG = 100
ERROR_MISSING_CSV = 101

EVAL_FILE_NAME = 'eval.txt'
CONFIG_FILE_NAME = 'CSMakefile.yml'
CSV_EVAL_REGEX = r'^HW.*\.csv$'
CSV_POINTS_NAME = 'points.csv'
ZIP_FILE_NAME = 'BRUTE.zip'

def command_delete(working_dir):
    for f in working_dir:
        if f == ZIP_FILE_NAME:
            print(f'removing: {f}')
            os.remove(f)
        elif f == CSV_POINTS_NAME:
            print(f'removing: {f}')
            os.remove(f)
        elif re.match(r'.*\.txt$',f):
            print(f'removing: {f}')
            os.remove(f)

def get_csv_file(working_dir):
    matches = map(lambda f : re.match(CSV_EVAL_REGEX, f), working_dir)
    matches = filter(lambda x:x, matches)
    file_name = next(matches)
    file_name = file_name.group() if file_name else ''
    return file_name

def load_data(working_dir):
    if not CONFIG_FILE_NAME in working_dir or not os.path.isfile(CONFIG_FILE_NAME):
        print_err(f'Missing {CONFIG_FILE_NAME}.')
        exit(ERROR_MISSING_CONFIG)

    csv_file_name = get_csv_file(working_dir)
    if not csv_file_name or not os.path.isfile(csv_file_name):
        print_err('Missing .csv with evaluation.')
        exit(ERROR_MISSING_CSV)

    with open(CONFIG_FILE_NAME, 'r') as config_file:
        config = yaml.safe_load(config_file)

    with open(csv_file_name, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        results_table = [ r for r in reader ]

    return config, results_table

def process_eval(config, results_table):
    student_penalty = []
    for result in results_table:
        login = result['Login']
        if os.path.isdir(login):
            ae_points = float(result['AE score'])
            tokens = tp.parse_dir(login)
            if not tokens:
                print_warn(f'No tokens present for student {login}')
            for t in tokens:
                tp.parse_token(t)

            eval_file = login + '.txt'
            penalty, _ = rp.output_txt(tokens, config, eval_file)

            student_penalty.append((login, ae_points*penalty))
            
            subprocess.run(['zip', ZIP_FILE_NAME, eval_file], capture_output=True) 
    
    return student_penalty

if __name__=='__main__':
    working_dir = os.listdir('.')

    if len(sys.argv) > 1:
        command = sys.argv[1]
        if command == 'clean':
            command_delete(working_dir)
            exit(0)
        else:
            print_err(f'Unknown command: {command}')
            exit(ERROR_UNKNOWN_COMMAND)
    
    config, results_table = load_data(working_dir)

    student_penalty = process_eval(config, results_table)

    with open(CSV_POINTS_NAME, 'w') as of:
        for login, penalty in student_penalty:
            of.write(f'{login},-{penalty}\n')
    subprocess.run(['zip', ZIP_FILE_NAME, CSV_POINTS_NAME], capture_output=True)
