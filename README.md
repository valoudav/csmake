# CSMake

CSMake is a utility make for CTU teachers of programming in C used.
It reduces overhead related to manual home work evaluation.

## Installation
Dependencies:

- python3
- zip

Clone the CMake repository and symlink the `csmake.py` exetutable somehwere on your `PATH`.

For example:

```
mkdir -p ~/software
cd ~/software
git clone https://gitlab.fel.cvut.cz/valoudav/csmake.git
ln -s ~/software/csmake/csmake.py ~/.local/bin/csmake
```

## Usage
1. Create a working directory for the evaluated homework.
2. Download all submissions from all your parallel classes, unzip all the data into the working directory.  
   <img title="a title" alt="Alt text" src="img/hw-link.png">  
   <img title="a title" alt="Alt text" src="img/uploads-link.png">
3. Download `.csv` file with the summary of AE results into the working directory.  
   <img title="a title" alt="Alt text" src="img/csv-link.png">
4. Create a `CSMakefile.yml` file inside the working directory.  
   An sample file is located in the `examples` directory.
5. Create 'magic comments' in students code (see. below).
6. run `csmake` inside the working directory.  
   The program will generate evaluation file for each student and a `.scv` file with point evaluation.  
   Moreover, a `BRUTE.zip` file is generated which can be directly uploaded to BRUTE.  
   <img title="a title" alt="Alt text" src="img/eval-link.png">


## Magic Comments
The core feature of CMake is the ability to extract 'magic comments' from C code.
A magic comments is a single line C comment containing evaluation tokens which are prefixed by the `$` symbol.
The `$` symbol was chose becaue it does not naturally occur insided a normal C code and resembles the § symbol.
The typical form of the evaluation token is:
```
$<rule no.>;<severity>;<comment>
```
The `<rule no.>` corresponds to the rules in [Pravidla pro psaní čitelného kódu](https://cw.fel.cvut.cz/wiki/courses/b3b36prg/resources/codingstyle/start).  
The `<severity>` is a number between 0 and 1. It determines how severe the penalty for the error should be.  
The `<comment>` can be used by the teacher to include any further explanation of the error.

There is a second form of the token:
```
$<comment>
```
This form serves to make comments on the code which do not fall under any of the defined rules.

## The CSMakefile.yml
The `CSMakefile.yml` contains a user defined configuration for CSMake.  
As of now it contains the `base_points` fields which defines the maximum points for the homework and the `penal_code` field.  
The `penal_code` field contains subfields which define the marked rules; including the weight of those rules and how they will be named in the automatically generated evaluation files.

## Acknowledgment
A recognition goes to Martin Zoula (zoulamar@fel.cvut.cz) who extended the original implementation of the CSMake tool.
Parts of this final version are inspired by ideas introduced by Martin.

## TODO List [by Martin Zoula]
- Investigate if it is possible to bulk-wise set the evaluated students to block their upload so that our CS evaluation won't get overriden? Or does it get overriden by re-upload at all?
- Automated UTF-8 check
- Automated malloc check
- Automated indentation check
- Automated braces check
- Some global notes and remarks embedded for every student to see
- Add warning for not flattened source files.



<!--


## Expected Workflow:
1. Download all submissions from all your parallel classes, unzip all the data into `data` directory. Expected directory listing:
    - `./code_review.py`
    - `./data/`
        - `potterh/'
            - `main.c'
            - `expeliarmus.c'
            - `expeliarmus.h'
        - `grangeh3/`
            - `main.c'
            - `petrificus_totallus.c'
            - `petrificus_totallus.h'
        - 'weaslyro/`
            - `main.c'
            - `eat_slugs.c'
            - `eat_slugs.h'
        - 'malfodra/`
            - `__MACOSX__/' Hmmm, zazobanec :-D A stejně mu to nebude fungovat ;-D
            - `main.c'
            - `what_does_he_cast?.c'
            - `what_does_he_cast?.c'
    - ...
- Manually check all submissions using _magic comments_, see below.
    - When student submits some nested folder, please manually flatten it! Overything deeper in student's floder will be ignored!
    - Of course, first compile and run the student's code if required.
- Run `make sanity_check`. Fix problems if you see any.
- Link selected yaml configuration (folder `configs`) to root (`ln -sf configs/<some-config>.yml config.yml`)
    - Feel free to edit a config if you thik so...
- Run `make` in root directory of this repo. This will compute evaluaiton points according to default yaml file.
- Upload `output/_BRUTE_UPLOAD.zip` to Brute.
- Keep `output/KEEP_ME_DATA.zip` in some safe place.
    This contains your long tedious and precious work.
    Perhaps print on some soft and soaky paper and later use it for The Purpose it really serves.
- Then keep an eye for someone who re-uploads...

## Dependencies

```
python3, LaTeX
```
This tool requires you to install a LaTeX compiler.
You have to have the ```listings``` and ```todonotes``` LaTex packages.
I recommend running:

```
sudo apt-get install texlive-full
```

## Magic Comments

`code_review.py` is a simple C-to-PDF translator.
It transforms magic comments `\\$` into PDF notes and calculates the final coding style penalty according to penalties defined in `.yaml` file.
Usage is documented and accesible via `./code_review.py -h`.

The ```CONFING``` file is a `.yml` file. Its structure is self-explainable. See HW8 which is thoroughly commented.

Keep in mind, that the magic comment has the following syntax:
```
int a;  //$<category>,<severity> <comment> 
```
Where severity is an integer from 0 to 10
and category is an integer; the category has to be present in the config file.

Final score is calculated such that for each rule the most severe violation is
picked and penalty for that rule is ```-10 * severity * max_penalty %```.
The total penalty is sum of penalties for each rule capped at 100 %.

The magic comment does not have to feature the rule number and severity;
comment must be present always!
Check the example ```main.c``` file.

